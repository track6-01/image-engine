#include <Arduino.h>
#include <U8x8lib.h>
#define _int uint8_t

U8X8_SSD1306_128X64_NONAME_HW_I2C u8x8(U8X8_PIN_NONE);

_int mp[8][128];

void flush() {
  for (_int i = 0; !(i & 8); i ++)
    u8x8.drawTile(0, i, 16, mp[i]);
}

void setPixel(_int x, _int y, bool t) {
  if (x & 64 | y & 128) return;
  if (t) mp[x >> 3][y] |= 1 << (x & 7);
  else mp[x >> 3][y] &= 0xff ^ (1 << (x & 7));
}

bool getPixel(_int x, _int y) {
  if (x & 64 | y & 128) return 0;
  return mp[x >> 3][y] & (1 << (x & 7));
}

void _init(){
  u8x8.begin();
  u8x8.setFlipMode(1);
}

void setup(){
  _init();
}

void loop() {
  for (int i = 0; i < 64; i ++){
    for (int j = 0; j < 128 ; j++)
      setPixel(i, j, true);
    flush();
  }
}
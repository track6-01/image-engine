# Image Engine

#### 介绍
一个适用于arduino的图像引擎

#### 接口

```cpp
void _init() // 初始化
void setPixel(_int x, _int y, bool t) // 将(x,y)的像素写为亮(t == true) / 不亮(t == false)
bool getPixel(_int x, _int y) // 获取(x,y)像素的状态
void flush() // 刷新当前屏幕并绘制
```